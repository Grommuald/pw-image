#include <string>
#include <memory>
#include <thread>
#include <vector>

#ifndef APPLICATION_HPP
#define APPLICATION_HPP

class Image;
class Filter;

class Application
{
public:
    Application( const std::string& );
    ~Application();

    void Run();

private:
    std::shared_ptr< Image > m_originalImage;
    std::vector< std::unique_ptr< Filter > > m_filters;
    std::vector< std::thread > m_filtersThreads;
};

#endif
