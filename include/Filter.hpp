#include <memory>
#include <thread>
#include <string>
#include <vector>

#ifndef FILTER_HPP
#define FILTER_HPP

class Image;
struct Pixel;

class Filter
{
public:
    Filter( std::shared_ptr< Image >, const std::string& = "filter" );
    virtual ~Filter();

    void Run();
    std::string GetName() const;
    std::shared_ptr< Image > GetProcessedImage();

protected:
    virtual void ProcessRow( const uint32_t& );
    void AllocateProcessedImage();

    std::string m_name;

    std::shared_ptr< Image > m_originalImage;
    std::shared_ptr< Image > m_processedImage;

    std::vector< std::thread > m_pixelThreads;
    std::vector< Pixel > m_resultPixels;
};

#endif
