#include <string>
#include <memory>
#include <vector>

#ifndef IMAGE_HPP
#define IMAGE_HPP

struct Pixel
{
    uint8_t colors[3];
};

class Image
{
public:
    Image( const uint32_t&, const uint32_t& );

    uint32_t GetWidth() const;
    uint32_t GetHeight() const;
    uint64_t GetSize() const;
    std::string GetFilename() const;

    void SetWidth( const uint32_t& );
    void SetHeight( const uint32_t& );
    void SetFilename( const std::string& );

    const std::vector< Pixel >& GetPixelData() const;
    void SetPixelData( const std::vector< Pixel >& );

private:
    uint32_t m_width;
    uint32_t m_height;

    std::string m_filename;
    std::vector< Pixel > m_pixelData;
};

class ImageUtil
{
public:
    static std::shared_ptr< Image > LoadPPMImageFromFile( const std::string& );
    static bool SavePPMImageToFile( const std::string&, std::shared_ptr< Image > );
};



#endif
