#include "OrderStatisticFilter.hpp"

#ifndef MAX_FILTER_HPP
#define MAX_FILTER_HPP

class MaxFilter : public OrderStatisticFilter
{
public:
    MaxFilter( std::shared_ptr< Image > );
};

#endif
