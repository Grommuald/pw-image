#include "OrderStatisticFilter.hpp"

#ifndef MEDIAN_FILTER_HPP
#define MEDIAN_FILTER_HPP

class MedianFilter : public OrderStatisticFilter
{
public:
    MedianFilter( std::shared_ptr< Image > );
};

#endif
