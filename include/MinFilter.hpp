#include "OrderStatisticFilter.hpp"

#ifndef MIN_FILTER_HPP
#define MIN_FILTER_HPP

class MinFilter : public OrderStatisticFilter
{
public:
    MinFilter( std::shared_ptr< Image > );
};

#endif
