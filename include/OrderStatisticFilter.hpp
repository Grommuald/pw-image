#include "Filter.hpp"
#include <functional>

#ifndef ORDER_STATISTIC_FILTER_HPP
#define ORDER_STATISTIC_FILTER_HPP

class OrderStatisticFilter : public Filter
{
public:
    OrderStatisticFilter(
        std::shared_ptr< Image >,
        std::function< uint32_t( const std::vector< Pixel >& ) >,
        const std::string& = "os_filter"
    );
    virtual ~OrderStatisticFilter();

protected:
    virtual void ProcessRow( const uint32_t& );

    std::function< uint32_t( const std::vector< Pixel >& ) > m_predicate;
};

#endif
