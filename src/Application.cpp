#include "Application.hpp"
#include "Image.hpp"
#include "MedianFilter.hpp"
#include "MaxFilter.hpp"
#include "MinFilter.hpp"

#include <iostream>

Application::Application( const std::string& filename )
    :
    m_originalImage( ImageUtil::LoadPPMImageFromFile( filename ) )
{
    std::cout << "Application(): Begin. End.\n";

    m_filters.push_back( std::make_unique< MedianFilter >( m_originalImage ) );
    m_filters.push_back( std::make_unique< MaxFilter >( m_originalImage ) );
    m_filters.push_back( std::make_unique< MinFilter >( m_originalImage ) );

}

Application::~Application()
{
    std::cout << "~Application(): Waiting for threads to join...\n";
    for ( auto& i : m_filtersThreads )
        i.join();
    std::cout << "~Application(): Finished.\n";
}

void Application::Run()
{
    std::cout << "Application::Run(): Creating threads...\n";
    for ( auto& i : m_filters )
    {
        m_filtersThreads.emplace_back(
        [ this, &i ]
        {
            i->Run();
            std::cout << "Image processed... Now saving...\n";
            ImageUtil::SavePPMImageToFile( "output/" + i->GetName(), i->GetProcessedImage() );
        } );
    }
    std::cout << "Application::Run(): Threads running...\n";
}
