#include "Filter.hpp"
#include "Image.hpp"

Filter::Filter(
    std::shared_ptr< Image > image,
    const std::string& name )
    :
    m_name( name ),
    m_originalImage( image ),
    m_resultPixels( image->GetSize() )
{

}

Filter::~Filter()
{

}

std::string Filter::GetName() const
{
    return m_name;
}

void Filter::AllocateProcessedImage()
{
    m_processedImage = std::make_shared< Image >(
        m_originalImage->GetWidth(),
        m_originalImage->GetHeight()
    );
    m_processedImage->SetPixelData( m_resultPixels );
}

std::shared_ptr< Image > Filter::GetProcessedImage()
{
    AllocateProcessedImage();

    return m_processedImage;
}

void Filter::ProcessRow( const uint32_t& )
{

}

void Filter::Run()
{
    for ( uint32_t i = 1; i < m_originalImage->GetHeight() - 1; ++i )
        m_pixelThreads.emplace_back(
        [ this, i ]
        {
            ProcessRow( i );
        } );

    for ( auto& i : m_pixelThreads )
        i.join();
}
