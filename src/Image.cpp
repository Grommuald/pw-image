#include "Image.hpp"
#include <cstdio>
#include <cstring>
#include <iostream>

std::shared_ptr< Image > ImageUtil::LoadPPMImageFromFile( const std::string& inputFilename )
{
    FILE* inputFile = fopen( inputFilename.c_str(), "rb" );

    if ( inputFile == nullptr )
    {
        std::cout << "ImageUtil: Cannot open file: " + inputFilename + ".\n";
        return nullptr;
    }

    const uint16_t BUFFER_SIZE = 256;

    uint32_t imageWidth = 0, imageHeight = 0;
    uint32_t maxColorComponentValue = 0;

    char buff[BUFFER_SIZE];
    char* t = fgets( buff, BUFFER_SIZE, inputFile );

    if ( t == nullptr || strncmp( buff, "P6\n", 3) != 0 )
    {
        std::cout << "ImageUtil: Cannot read image header.\n";
        return nullptr;
    }

    do
    {
       if ( ( t = fgets( buff, BUFFER_SIZE, inputFile ) ) == nullptr )
       {
           std::cout << "ImageUtil: Cannot read into the buffer.\n";
           return nullptr;
       }

    } while ( strncmp( buff, "#", 1 ) == 0 );

    if ( sscanf( buff, "%u %u", &imageWidth, &imageHeight ) < 2 )
    {
        std::cout << "ImageUtil: Cannot read image size.\n";
        return nullptr;
    }


    if ( fscanf( inputFile, "%u", &maxColorComponentValue ) < 1
            || maxColorComponentValue != 255 )
    {
        std::cout << "ImageUtil: Cannot read max color component value.\n";
        return nullptr;
    }

    fseek( inputFile, 1, SEEK_CUR );

    std::shared_ptr< Image > loadedImage = std::make_shared< Image >( imageWidth, imageHeight );
    std::vector< Pixel > pixelData;

    Pixel currentPixel = { 0, 0, 0 };

    while ( fread( &currentPixel, sizeof( Pixel ), 1, inputFile ) )
        pixelData.push_back( currentPixel );

    loadedImage->SetPixelData( pixelData );
    fclose( inputFile );

    return loadedImage;
}

bool ImageUtil::SavePPMImageToFile( const std::string& filename, std::shared_ptr< Image > inputImage )
{
    if ( inputImage == nullptr )
    {
        std::cout << "ImageUtil: Image pointer is null.\n";
        return true;
    }

    std::string filenameWithExtension = filename + ".ppm";
    FILE* outputFile = fopen( filenameWithExtension.c_str(), "wb" );

    if ( outputFile == nullptr )
    {
        std::cout << "ImageUtil: Cannot open file: " + filenameWithExtension + ".\n";
        return true;
    }
    fprintf( outputFile, "P6\n %s\n %d\n %d\n %d\n",
        "", inputImage->GetWidth(), inputImage->GetHeight(), 255 );

    for ( const auto& i : inputImage->GetPixelData() )
        fwrite( ( void* ) &i, 1, sizeof( Pixel ), outputFile );

    fclose( outputFile );

    return false;
}

Image::Image( const uint32_t& width, const uint32_t& height )
    :
    m_width( width ),
    m_height( height )
{

}

uint32_t Image::GetWidth()       const { return m_width; }
uint32_t Image::GetHeight()      const { return m_height; }
uint64_t Image::GetSize()        const { return m_width * m_height; }
std::string Image::GetFilename() const { return m_filename; }

void Image::SetWidth( const uint32_t& width )
{
    m_width = width;
}

void Image::SetHeight( const uint32_t& height )
{
    m_height = height;
}

void Image::SetFilename( const std::string& filename )
{
    m_filename = filename;
}

const std::vector< Pixel >& Image::GetPixelData() const
{
    return m_pixelData;
}

void Image::SetPixelData( const std::vector< Pixel >& pixelData )
{
    m_pixelData = pixelData;
}
