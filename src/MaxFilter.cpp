#include "MaxFilter.hpp"
#include "Image.hpp"

MaxFilter::MaxFilter( std::shared_ptr< Image > image )
    :
    OrderStatisticFilter(
        image,
        [] ( const std::vector< Pixel >& pixels )
        {
            return pixels.size() - 1;
        },
        "max_filter" )
{

}
