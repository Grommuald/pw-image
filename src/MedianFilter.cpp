#include "MedianFilter.hpp"
#include "Image.hpp"

MedianFilter::MedianFilter( std::shared_ptr< Image > image )
    :
    OrderStatisticFilter(
        image,
        [] ( const std::vector< Pixel >& pixels )
        {
            return pixels.size() / 2;
        },
        "median_filter" )
{

}
