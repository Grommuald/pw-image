#include "MinFilter.hpp"
#include "Image.hpp"

MinFilter::MinFilter( std::shared_ptr< Image > image )
    :
    OrderStatisticFilter(
        image,
        [] ( const std::vector< Pixel >& pixels )
        {
            return 0;
        },
        "min_filter" )
{

}
