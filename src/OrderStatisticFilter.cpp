#include "OrderStatisticFilter.hpp"
#include "Image.hpp"

OrderStatisticFilter::OrderStatisticFilter(
    std::shared_ptr< Image > image,
    std::function< uint32_t( const std::vector< Pixel >& ) > predicate,
    const std::string& name
)
    :
    Filter( image, name ),
    m_predicate( predicate )
{

}

OrderStatisticFilter::~OrderStatisticFilter()
{

}

void OrderStatisticFilter::ProcessRow( const uint32_t& row )
{
    for ( uint32_t col = 1; col < m_originalImage->GetWidth() - 1; ++col )
    {
        std::vector< Pixel > pixels =
    	{
    		m_originalImage->GetPixelData()[( row - 1 ) * m_originalImage->GetWidth() + col - 1],
    		m_originalImage->GetPixelData()[( row - 1 ) * m_originalImage->GetWidth() + col],
    		m_originalImage->GetPixelData()[( row - 1 ) * m_originalImage->GetWidth() + col + 1],
    		m_originalImage->GetPixelData()[row * m_originalImage->GetWidth() + col - 1],
    		m_originalImage->GetPixelData()[row * m_originalImage->GetWidth() + col],
    		m_originalImage->GetPixelData()[row * m_originalImage->GetWidth() + col + 1],
    		m_originalImage->GetPixelData()[( row + 1 ) * m_originalImage->GetWidth() + col - 1],
    		m_originalImage->GetPixelData()[( row + 1 ) * m_originalImage->GetWidth() + col],
    		m_originalImage->GetPixelData()[( row + 1 ) * m_originalImage->GetWidth() + col + 1]
    	};

    	for ( size_t k = 0; k < 3; ++k )
    	{
    		uint8_t m = 1;
    		while ( m < pixels.size() )
    		{
    			uint8_t n = m;
    			while ( n > 0 && pixels[n - 1].colors[k] > pixels[n].colors[k] )
    			{
                    std::swap( pixels[n - 1].colors[k], pixels[n].colors[k] );
    				--n;
    			}
    			++m;
    		}
    	}
        // only this line of code is interchangable between max, min and median filters
        m_resultPixels[row *  m_originalImage->GetWidth() + col] = pixels[ m_predicate( pixels ) ];
    }
}
