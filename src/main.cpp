#include "Application.hpp"

int main( int argc, char** argv )
{
    Application app( "res/image.ppm" );
    app.Run();

    return 0;
}
